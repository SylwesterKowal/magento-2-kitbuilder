<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Xcreator\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Config extends AbstractHelper
{

    const SECTIONS = 'xcreator';   // module name
    const GROUPS = 'settings';        // setup general

    /**
     * @var AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @var Random
     */
    protected $mathRandom;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\AuthorizationInterface $authorization
     * @param \Magento\Framework\Math\Random $random
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context      $context,
        \Magento\Framework\ObjectManagerInterface  $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\AuthorizationInterface  $authorization,
        \Magento\Framework\Math\Random             $random
    )
    {
        $this->_authorization = $authorization;
        $this->mathRandom = $random;
        $this->storeManager = $storeManager;

        parent::__construct($context);
    }


    /**
     * @return bool
     */
    public function isEnabled($store_id = 0)
    {
        $orderStatus = $this->getGeneralCfg('enable', $store_id);
        return $orderStatus;
    }


    public function getConfig($cfg = null, $store_id = 0)
    {
        return $this->scopeConfig->getValue(
            $cfg,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store_id
        );
    }

    public function getGeneralCfg($cfg = null, $store_id = 0)
    {
        $config = $this->scopeConfig->getValue(
            self::SECTIONS . '/' . self::GROUPS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store_id
        );

        if (isset($config[$cfg])) return $config[$cfg];
        return $config;
    }


    /**
     * @param Customer $customer
     *
     * @return StoreInterface|null
     * @throws NoSuchEntityException
     */
    public function getStore($customer = null)
    {
        if (!is_null($customer) && $storeId = $customer->getStoreId()) {
            return $this->storeManager->getStore($storeId);
        }

        return $this->storeManager->getDefaultStoreView();
    }
}

