<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Xcreator\Block\Index;

class Index extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Kowal\Xcreator\Helper\Config                    $config,
        array                                            $data = []
    )
    {
        $this->config = $config;
        parent::__construct($context, $data);
    }


    public function isEnable(){
        return $this->config->isEnabled($this->config->getStore());
    }

    public function getKitBuyilderScript(){
        return $this->config->getGeneralCfg('kit_builder_script', $this->config->getStore());
    }
}

